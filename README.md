# Behavior Tree Editor on Kotlin for [gdx-ai](https://github.com/libgdx/gdx-ai)

Based on project [Behavior Tree Editor v2](https://github.com/piotr-j/bte2)


This is mostly the same as libgdx project created with setup up, make sure you can run that without issues first.

```
repositories {
  maven(url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
}
   

dependencies {
  ...
  implementation("com.silent-bugs:bt-editor:0.8.0-SNAPSHOT")
}
```


### Screenshot
![editor screenshot](extras/bte.jpg)
