import org.jetbrains.kotlin.config.KotlinCompilerVersion

plugins {
    kotlin("jvm")
    `java-library`
    `maven-publish`
    signing
}

sourceSets {
    named("main") {
        java.setSrcDirs(
            listOf("src/main/java")
        )
    }
}

group = "com.silent-bugs"
version = "0.8.0-SNAPSHOT"

dependencies {
    implementation(kotlin("stdlib", KotlinCompilerVersion.VERSION))

    implementation("com.badlogicgames.gdx:gdx:${properties["version.gdx"]}")
    implementation("com.kotcrab.vis:vis-ui:${properties["version.visui"]}")
    implementation("com.badlogicgames.gdx:gdx-ai:${properties["version.gdx.ai"]}")
}
java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.register<Jar>("sourcesJar") {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allJava)
}

tasks.register<Jar>("javadocJar") {
    archiveClassifier.set("javadoc")
    from(tasks.javadoc.get().destinationDir)
}

publishing {
    repositories {
        maven {
            name = "MavenCentral"
            val releasesRepoUrl =
                "https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/"
            val snapshotsRepoUrl = "https://s01.oss.sonatype.org/content/repositories/snapshots/"
            url = uri(
                if (version.toString().endsWith("SNAPSHOT")) snapshotsRepoUrl else releasesRepoUrl
            )
            credentials {
                username = project.properties["mavenUploadUser"]?.toString() ?: ""
                password = project.properties["mavenUploadPwd"]?.toString() ?: ""
            }
        }
    }
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            artifact(tasks["sourcesJar"])
            artifact(tasks["javadocJar"])

            pom {
                name.set("Behavior Tree Editor")
                description.set("Behavior Tree Editor for gdx-ai")
                url.set("https://gitlab.com/silent-bugs/bt-editor")
                licenses {
                    license {
                        name.set("Apache License, Version 2.0")
                        url.set("https://gitlab.com/silent-bugs/bt-editor/-/blob/master/LICENSE.md")
                    }
                }
                developers {
                    developer {
                        name.set("Gidroshvandel")
                        email.set("gidroshvandel@gmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://gitlab.com/silent-bugs/bt-editor.git")
                    developerConnection.set("scm:git:https://gitlab.com/silent-bugs/bt-editor.git")
                    url.set("https://gitlab.com/silent-bugs/bt-editor")
                }
            }
        }
    }
}

signing {
    val pgpSigningKey: String = project.properties["pgpSigningKey"]?.toString() ?: ""
    val pgpSigningPassword: String = project.properties["pgpSigningPassword"]?.toString() ?: ""
    useInMemoryPgpKeys(pgpSigningKey, pgpSigningPassword)
    sign(publishing.publications["mavenJava"])
}
